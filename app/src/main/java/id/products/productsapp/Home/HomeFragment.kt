package id.products.productsapp.Home

import AdapterProducts
import android.annotation.SuppressLint
import android.app.ProgressDialog
import android.content.Intent
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import id.news.app.RestAPI.mC
import id.products.productsapp.DataHelper
import id.products.productsapp.R
import id.products.productsapp.Response

class HomeFragment : Fragment(), HomeView {

    lateinit var lin: LinearLayout
    lateinit var butsearch: Button
    lateinit var presenter: HomePreenter
    lateinit var pDialog: ProgressDialog
    lateinit var response: TextView
    lateinit var data: RecyclerView
    lateinit var dbcenter: DataHelper
    lateinit var db: SQLiteDatabase
    lateinit var cursor: Cursor

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    @SuppressLint("SetTextI18n", "ClickableViewAccessibility")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        lin = view.findViewById(R.id.lin)
        butsearch = view.findViewById(R.id.btnsearch)
        response = view.findViewById(R.id.response)
        response.isVisible = false
        data = view.findViewById(R.id.data)
        data.isVisible = false
        hide()

        presenter = HomePreenter(this)

        lin.setOnTouchListener { v, event ->
            hide()
            false
        }
        butsearch.setOnClickListener {
            hide()
            composeLink()
        }
        response.setOnClickListener {

            clickresponse()
        }

    }

    private fun composeLink() {
        response.isVisible = false
        data.isVisible = false
        presenter.getProducts()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
    }

    companion object {
        const val TAG = "HomeFragment"
        lateinit var valRes: ProductRes
    }

    private fun hide() {
        val inputManager = this.requireActivity().getSystemService(
            AppCompatActivity.INPUT_METHOD_SERVICE
        ) as InputMethodManager
        val focusedView = this.requireActivity().currentFocus
        if (focusedView != null) {
            inputManager.hideSoftInputFromWindow(
                focusedView.windowToken,
                InputMethodManager.HIDE_NOT_ALWAYS
            )
        }
    }

    override fun onload() {
        pDialog = ProgressDialog(requireActivity())
        pDialog.setCancelable(false)
        pDialog.show()
        pDialog.window!!.setLayout(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.MATCH_PARENT
        )
        pDialog.window!!.setGravity(Gravity.CENTER or Gravity.CENTER_HORIZONTAL or Gravity.CENTER_VERTICAL)
        pDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        pDialog.setContentView(R.layout.layout_loader)
    }

    override fun stopload() {
        pDialog.dismiss()
    }

    override fun onfailed(message: String) {
        Toast.makeText(requireActivity(), message, Toast.LENGTH_SHORT).show()
        response.isVisible = false
        data.isVisible = false
    }

    override fun onsucess(res: ProductRes) {
        response.isVisible = true
        createlist(res)
        valRes = res

        savetoSqlite(res)
    }

    private fun savetoSqlite(res: ProductRes) {
        dbcenter = DataHelper(requireActivity())
        db = dbcenter.readableDatabase
        db = dbcenter.writableDatabase
//        db.execSQL("DELETE FROM kegiatan WHERE tglygdiambil < datetime('now','-1 day','localtime')")
        cursor = db.rawQuery("SELECT * FROM products", null)

        cursor.moveToFirst()

        if (cursor.count > 0) {
            Log.e("cursor", cursor.count.toString())
            for (i in 0 until res.products.size) {
                for (cc in 0 until cursor.count) {
                    cursor.moveToPosition(cc)
                    if (res.products[i].id == cursor.getInt(1)) {
                        return
                    } else {
                        //insert

                        Log.e(
                            "sdsdsdsd",
                            res.products[i].id.toString() + "()()()()" + cursor.getInt(1).toString()
                        )
                        var imgToInsert: String =
                            res.products[i].images.toString().replace("[", "").replace(",", ",,")
                                .replace("]", "").trim()
                        db.execSQL(
                            "INSERT INTO products(idproduct,title,description,price,discountPercentage,rating,stock,brand,category,thumbnail,images) values('" +
                                    res.products[i].id + "','" +
                                    res.products[i].title.replace("'", "%") + "','" +
                                    res.products[i].description.replace("'", "%") + "','" +
                                    res.products[i].price + "','" +
                                    res.products[i].discountPercentage + "','" +
                                    res.products[i].rating + "','" +
                                    res.products[i].stock + "','" +
                                    res.products[i].brand.replace("'", "%") + "','" +
                                    res.products[i].category.replace("'", "%") + "','" +
                                    res.products[i].thumbnail + "','" +
                                    imgToInsert + "')"
                        )


                    }
                }
            }

        } else {
            //insert
            for (i in 0 until res.products.size) {

                //insert
                var imgToInsert: String =
                    res.products[i].images.toString().replace("[", "").replace(",", ",,")
                        .replace("]", "").trim()

                db.execSQL(
                    "INSERT INTO products(idproduct,title,description,price,discountPercentage,rating,stock,brand,category,thumbnail,images) values('" +
                            res.products[i].id + "','" +
                            res.products[i].title.replace("'", "%") + "','" +
                            res.products[i].description.replace("'", "%") + "','" +
                            res.products[i].price + "','" +
                            res.products[i].discountPercentage + "','" +
                            res.products[i].rating + "','" +
                            res.products[i].stock + "','" +
                            res.products[i].brand.replace("'", "%") + "','" +
                            res.products[i].category.replace("'", "%") + "','" +
                            res.products[i].thumbnail + "','" +
                            imgToInsert + "')"
                )

            }
        }

//        pDialog.dismiss()
    }

    private fun clickresponse() {
        val intent = Intent(requireActivity(), Response::class.java)
        startActivity(intent)
        requireActivity().overridePendingTransition(
            R.anim.slide_in_right,
            R.anim.slide_out_left
        )
    }

    private fun createlist(res: ProductRes) {
        data.isVisible = true

        var datanya = ArrayList<dataproduk>()
        for (j in 0 until res.products.size) {

            datanya.add(
                dataproduk(
                    res.products[j].id,
                    res.products[j].title,
                    res.products[j].description,
                    res.products[j].price,
                    res.products[j].discountPercentage,
                    res.products[j].rating,
                    res.products[j].stock,
                    res.products[j].brand,
                    res.products[j].category,
                    res.products[j].thumbnail,
                    res.products[j].images
                )
            )


        }

        val adapter = AdapterProducts(datanya)
        data.setHasFixedSize(false)
        val layoutManager = GridLayoutManager(requireActivity(), 2)
        data.layoutManager = layoutManager
        data.suppressLayout(true)
        data.isNestedScrollingEnabled = true
        data.adapter = adapter

    }
}