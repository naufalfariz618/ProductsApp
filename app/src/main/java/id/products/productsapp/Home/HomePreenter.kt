package id.products.productsapp.Home

import android.util.Log
import id.news.app.RestAPI.ApiService
import id.products.productsapp.RestAPI.NetworkClient
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class HomePreenter(private val mvp: HomeView) {
    private val compositeDisposable = CompositeDisposable()

    fun getProducts() {
        mvp.onload()
        compositeDisposable.add(
            NetworkClient.getClient().create(ApiService::class.java)
                .getproducts(100)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { res ->
                    mvp.stopload()
                    if (res.total==0) {
//                        mvp.stopload()
                        mvp.onfailed("Products is Empty")
                    }else{
                       mvp.onsucess(res)
                    }
                }
        )
    }


}