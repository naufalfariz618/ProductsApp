package id.products.productsapp.Home

import java.util.ArrayList

interface HomeView {
    fun onload()
    fun stopload()
    fun onfailed(message: String)
    fun onsucess(res: ProductRes)

}