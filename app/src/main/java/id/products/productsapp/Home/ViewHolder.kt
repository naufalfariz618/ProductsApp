package id.products.productsapp.Home

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.graphics.Paint
import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.load.resource.bitmap.CenterInside
import com.bumptech.glide.load.resource.bitmap.GranularRoundedCorners
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import id.products.productsapp.DetailProducts
import id.products.productsapp.R
import id.products.productsapp.Response
import java.text.DecimalFormat
import java.util.*
import kotlin.collections.ArrayList


class ViewHolder(inflater: LayoutInflater,parent: ViewGroup): RecyclerView.ViewHolder(inflater.inflate(
    R.layout.listview_products, parent, false)) {

    lateinit var foto: ImageView
    lateinit var nama: TextView
    lateinit var harga: TextView
    lateinit var disc: TextView
    lateinit var hargaslash: TextView
    lateinit var brand: TextView
    lateinit var rating: TextView
    lateinit var kotakdisc: RelativeLayout
    lateinit var maincontent: LinearLayout
    var act: Activity

    init {
        foto = itemView.findViewById(R.id.foto)
        nama = itemView.findViewById(R.id.txnama)
        harga = itemView.findViewById(R.id.txharga)
        disc = itemView.findViewById(R.id.txdisc)
        hargaslash = itemView.findViewById(R.id.txhargaslash)
        brand = itemView.findViewById(R.id.txtag)
        rating = itemView.findViewById(R.id.txrating)
        kotakdisc = itemView.findViewById(R.id.kotakdisc)
        maincontent = itemView.findViewById(R.id.maincontent)
        act = itemView.context as Activity
    }


    @SuppressLint("SetTextI18n")
    fun bind(data : dataproduk) {

        nama.text = data.title
        val stringp1: String = DecimalFormat("#,###,###").format(data.price.toDouble())

        if (data.disc==0.0){
            kotakdisc.visibility = View.GONE
            hargaslash.visibility = View.GONE
            harga.text = "Rp. " + stringp1.replace(",", ".")
        }else{
            kotakdisc.visibility = View.VISIBLE
            hargaslash.visibility = View.VISIBLE
            val priceafter = data.price.toDouble()-(data.price.toDouble()*(data.disc/100))
            hargaslash.text = "Rp. " + stringp1.replace(",", ".")
            val stringp2: String = DecimalFormat("#,###,###").format(priceafter)
            harga.text = "Rp. " + stringp2.replace(",", ".")
        }
        disc.text = data.disc.toString()+" %"
        hargaslash.paintFlags = hargaslash.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
        brand.text = data.brand
        rating.text = data.rating.toString()

        Glide.with(itemView.context)
            .load(data.thumbnail)
            .placeholder(R.drawable.imagedefault)
            .error(R.drawable.imagedefault)
            .skipMemoryCache(true)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .apply(
                RequestOptions.centerInsideTransform()
                    .transform(CenterInside(), GranularRoundedCorners(20F, 20F, 0F, 0F))
            )
            .listener(object : RequestListener<Drawable?> {
                override fun onLoadFailed(
                    e: GlideException?,
                    model: Any,
                    target: Target<Drawable?>,
                    isFirstResource: Boolean
                ): Boolean {
                    return false
                }

                override fun onResourceReady(
                    resource: Drawable?,
                    model: Any,
                    target: Target<Drawable?>,
                    dataSource: DataSource,
                    isFirstResource: Boolean
                ): Boolean {
                    return false
                }
            })
            .into(foto)

        maincontent.setOnClickListener{
            val intent = Intent(itemView.context, DetailProducts::class.java)
            intent.putExtra("desc",data.desc)
            val array: ArrayList<String> = ArrayList()
            array.addAll(data.images)
            intent.putStringArrayListExtra("image",array)
            itemView.context.startActivity(intent)
            act.overridePendingTransition(
                R.anim.slide_in_right,
                R.anim.slide_out_left
            )
        }

//        if (data.disc == "" ||data.disc == "0"){
//            kotakdisc!!.visibility = View.GONE
//            hargaslash!!.visibility = View.GONE
//        }else{
//            kotakdisc!!.visibility = View.VISIBLE
//            hargaslash!!.visibility = View.VISIBLE
//
//        }
//
//        nama?.text = data.name
//        val localeID = Locale("in", "ID")
//        val format = NumberFormat.getInstance(localeID) as DecimalFormat
//        format.applyPattern("#,###.##")
//        val hargafix = data.price.toDouble()
//        harga?.text = "Rp "+format.format(hargafix)
//        disc?.text = data.disc+"%"
//        val hargaslasfix = data.slash.toDouble()
//        hargaslash?.text = "Rp "+format.format(hargaslasfix)
//        hargaslash!!.paintFlags = hargaslash!!.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
//        loc?.text = data.cityshop
//        store?.text = data.nameshop
//
//        Glide.with(itemView.context)
//            .load(data.image)
//            .placeholder(R.drawable.imagedefault)
//            .error(R.drawable.imagedefault)
//            .skipMemoryCache(true)
//            .diskCacheStrategy(DiskCacheStrategy.ALL)
//            .apply(
//                RequestOptions.centerInsideTransform()
//                    .transform(CenterInside(), GranularRoundedCorners(20F, 20F, 0F, 0F))
//            )
//            .listener(object : RequestListener<Drawable?> {
//                override fun onLoadFailed(
//                    e: GlideException?,
//                    model: Any,
//                    target: Target<Drawable?>,
//                    isFirstResource: Boolean
//                ): Boolean {
//                    return false
//                }
//
//                override fun onResourceReady(
//                    resource: Drawable?,
//                    model: Any,
//                    target: Target<Drawable?>,
//                    dataSource: DataSource,
//                    isFirstResource: Boolean
//                ): Boolean {
//                    return false
//                }
//            })
//            .into(foto!!)
    }
}