package id.news.app.RestAPI

import android.content.Context
import android.net.ConnectivityManager
import id.products.productsapp.RestAPI.NoConnectivityException
import okhttp3.*
import java.io.IOException

class NetworkConnectionInterceptor(private val mContext: Context) : Interceptor {
    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        if (!isConnected) throw NoConnectivityException()
        var request = chain.request()
        try {
            val requestBuilder = request.newBuilder()
//                .addHeader("Authorization", BuildConfig.apikey)
                .addHeader("deviceplatform", "android")
                .addHeader("Content-Type", "application/json")
            request = requestBuilder.build()
        } catch (e: Exception) {
            e.printStackTrace()
        }

        val response = chain.proceed(request)
        val code = response.code
        var stringData = ""
        response.body?.string()?.let { json ->
            stringData = json
        }
        val contentType = response.body?.contentType()
        val body = ResponseBody.create(contentType, stringData)

        return response.newBuilder().code(if (code >= 500) code else 200).body(body).build()
    }

    private val isConnected: Boolean
        get() {
            val connectivityManager =
                mContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val netInfo = connectivityManager.activeNetworkInfo
            return netInfo != null && netInfo.isConnected
        }
}