package id.products.productsapp.RestAPI;

import android.app.Activity;
import android.app.Application;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.List;

public class App extends Application {
    private static App INSTANCE;
 
    private String unable;
    private Activity activity;

    public Activity getActivity() {
        return activity;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    public static App getInstance() {
        return INSTANCE;
    }

    public static List<AppCompatActivity> activities;

    public String getUnable() {
        return unable;
    }

    public void setUnable(String unable) {
        this.unable = unable;
    }

    public static void addActivity(AppCompatActivity activity) {
        if (activities == null) {
            activities = new ArrayList<>();
        }
        activities.add(activity);
    }

    public static void finishAll() {
        for (AppCompatActivity activity : activities) {
            if (!activity.isFinishing()) {
                activity.finish();
            }
        }
    }

    // 移除当前activity之前所有的activity，并finish
    public static void removeBefore() {
        if (activities.isEmpty()) {
            return;
        }
        try {
            AppCompatActivity lastActivity = activities.remove(activities.size() - 1);
            int size = activities.size();
            for (int i = size - 1; i >= 0; i--) {
                Activity temp = activities.get(i);
                temp.finish();
            }
            activities.clear();
            activities.add(lastActivity);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        INSTANCE = this;
    }
}
