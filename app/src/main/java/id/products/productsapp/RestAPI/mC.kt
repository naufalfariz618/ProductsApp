package id.news.app.RestAPI

import id.products.productsapp.BuildConfig
import javax.crypto.Cipher
import javax.crypto.spec.IvParameterSpec
import javax.crypto.spec.SecretKeySpec

/**
 * Created by DIARK-PC on 12/11/2017.
 */
class mC {
    fun encrypt(data: String): String {
        try {
            val cipher = Cipher.getInstance("AES/CBC/PKCS7Padding")
            val secretKeySpec = SecretKeySpec(SecretKey.toByteArray(), "AES")
            val ivps = IvParameterSpec(iv.toByteArray())
            cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec, ivps)
            return bytesToHex(cipher.doFinal(data.toByteArray()))!!
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return ""
    }

    fun decrypt(data: String): String {
        try {
            val cipher = Cipher.getInstance("AES/CBC/PKCS7Padding")
            val secretKeySpec = SecretKeySpec(SecretKey.toByteArray(), "AES")
            val ivps = IvParameterSpec(iv.toByteArray())
            cipher.init(Cipher.DECRYPT_MODE, secretKeySpec, ivps)
            return String(cipher.doFinal(hexToBytes(data)))
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return ""
    }

    companion object {
        private val iv = BuildConfig.iv
        private val SecretKey = BuildConfig.secretkey
        private fun hexToBytes(str: String?): ByteArray? {
            return if (str == null) {
                null
            } else if (str.length < 2) {
                null
            } else {
                val len = str.length / 2
                val buffer = ByteArray(len)
                for (i in 0 until len) {
                    buffer[i] = str.substring(i * 2, i * 2 + 2).toInt(16).toByte()
                }
                buffer
            }
        }

        private fun bytesToHex(data: ByteArray?): String? {
            if (data == null) {
                return null
            }
            val len = data.size
            var str = ""
            for (i in 0 until len) {
                str = if (data[i].toInt() and 0xFF < 16) str + "0" + Integer.toHexString(
                    data[i].toInt() and 0xFF
                ) else str + Integer.toHexString(data[i].toInt() and 0xFF)
            }
            return str
        }
    }
}