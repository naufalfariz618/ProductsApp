package id.news.app.RestAPI

//import id.news.app.SubMenu.IsiKategori.isikategoriResponse
import id.products.productsapp.Home.ProductRes
import io.reactivex.Observable
import retrofit2.http.*

interface ApiService {
    @GET("/products")
    fun getproducts(
        @Query("limit") limit: Int
    ): Observable<ProductRes>
}