package id.products.productsapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageButton
import android.widget.TextView
import id.products.productsapp.Home.HomeFragment

class Response : AppCompatActivity() {
    lateinit var back: ImageButton
    lateinit var txres: TextView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_response)

        window.statusBarColor = resources.getColor(R.color.biru)

        back = findViewById(R.id.icback)
        back.setOnClickListener { onBackPressed() }
        txres = findViewById(R.id.txres)
        txres.text = HomeFragment.valRes.toString()


    }

    override fun onBackPressed() {
        super.onBackPressed()
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right)
    }
}