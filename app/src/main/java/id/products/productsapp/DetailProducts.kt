package id.products.productsapp

import android.os.Bundle
import android.widget.ImageButton
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.viewpager.widget.ViewPager
import androidx.viewpager.widget.ViewPager.OnPageChangeListener
import com.viewpagerindicator.CirclePageIndicator

class DetailProducts : AppCompatActivity() {
    lateinit var back: ImageButton
    lateinit var txdesc: TextView
    lateinit var mPager: ViewPager
    lateinit var urls: ArrayList<String>
    private var NUM_PAGES = 0
    private var currentPage = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_products)

        window.statusBarColor = resources.getColor(R.color.biru)
        val desc:String = intent.getStringExtra("desc").toString()
        val image = intent.getStringArrayListExtra("image")
        if (image != null) {
            urls = image
        }

        back = findViewById(R.id.icback)
        back.setOnClickListener { onBackPressed() }
        txdesc = findViewById(R.id.txdesc)
        txdesc.text = desc

        init()


    }

    private fun init() {
        mPager = findViewById(R.id.pager)
        mPager.adapter = slidegbr(
            this,
            urls
        )

        NUM_PAGES = urls.size

    }

    override fun onBackPressed() {
        super.onBackPressed()
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right)
    }
}