package id.products.productsapp

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.os.Handler
import android.provider.MediaStore
import android.util.Base64
import android.view.Menu
import android.view.inputmethod.InputMethodManager
import android.widget.FrameLayout
import android.widget.Toast
import id.products.productsapp.Home.HomeFragment
import id.products.productsapp.Menu.MenuFragment
import nl.joery.animatedbottombar.AnimatedBottomBar
import java.io.*

class MainActivity : AppCompatActivity(),AnimatedBottomBar.OnTabInterceptListener {

    var doubleBackToExitPressedOnce = false
    lateinit var navbar: AnimatedBottomBar
    lateinit var frameLayout: FrameLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        window.statusBarColor = resources.getColor(R.color.biru)

        navbar = findViewById<AnimatedBottomBar>(R.id.bottom_bar)
        navbar.setOnTabInterceptListener(this)
        frameLayout = findViewById<FrameLayout>(R.id.fragment_layout)

        if (savedInstanceState == null) {
            showFragment(HomeFragment.TAG)
        }
    }

    private fun showFragment(tag: String) {
        var fragment = supportFragmentManager.findFragmentByTag(tag)
        if (fragment == null) {
            when (tag) {
                HomeFragment.TAG -> {
                    fragment = HomeFragment()
                }
                else -> {
                    fragment = MenuFragment()
                }
            }
        }
        supportFragmentManager
            .beginTransaction()
            .replace(frameLayout.getId(), fragment!!, tag)
            .commit()
    }

    override fun onTabIntercepted(
        lastIndex: Int,
        lastTab: AnimatedBottomBar.Tab?,
        newIndex: Int,
        newTab: AnimatedBottomBar.Tab
    ): Boolean {
        when (newTab.id) {
            R.id.home -> {
                showFragment(HomeFragment.TAG)
                return true
            }
            R.id.menu -> {
                showFragment(MenuFragment.TAG)
                return true
            }
        }
        return false
    }

    override fun onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed()
            return
        }
        doubleBackToExitPressedOnce = true
        Toast.makeText(this, "Click again to exit", Toast.LENGTH_SHORT).show()
        Handler().postDelayed({ doubleBackToExitPressedOnce = false }, 2000)
    }
}