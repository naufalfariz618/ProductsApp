package id.products.productsapp

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.github.chrisbanes.photoview.PhotoView
import id.products.productsapp.R

class ZoomPhoto : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_zoom_photo)
        window.statusBarColor = resources.getColor(R.color.biru)


        val image:String = intent.getStringExtra("gambar").toString()

        val photoView = findViewById<PhotoView>(R.id.photo)
        val imageuri: String = image
        Glide.with(baseContext)
            .load(imageuri)
            .error(R.drawable.imagedefault)
            .placeholder(R.drawable.imagedefault)
            .fitCenter()
            .skipMemoryCache(true)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .into(photoView)
    }
}