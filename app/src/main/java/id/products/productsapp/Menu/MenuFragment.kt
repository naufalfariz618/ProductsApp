package id.products.productsapp.Menu

import AdapterProducts
import android.annotation.SuppressLint
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.util.Log
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import id.products.productsapp.DataHelper
import id.products.productsapp.Home.HomePreenter
import id.products.productsapp.Home.dataproduk
import id.products.productsapp.R

class MenuFragment : Fragment() {

    lateinit var lin: LinearLayout
    lateinit var butsearch: Button
    lateinit var data: RecyclerView
    lateinit var edkeyword: EditText
    lateinit var dbcenter: DataHelper
    lateinit var db: SQLiteDatabase
    lateinit var cursor: Cursor

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_menu, container, false)
    }

    @SuppressLint("SetTextI18n", "ClickableViewAccessibility")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        lin = view.findViewById(R.id.lin)
        butsearch = view.findViewById(R.id.btnsearch)
        data = view.findViewById(R.id.data)
        data.isVisible = false
        edkeyword = view.findViewById(R.id.edkeyword)
        hide()

        lin.setOnTouchListener { v, event ->
            hide()
            false
        }
        butsearch.setOnClickListener {
            hide()
            searchfromSqlite(edkeyword.text.toString())
        }

    }

    private fun hide() {
        val inputManager = this.requireActivity().getSystemService(
            AppCompatActivity.INPUT_METHOD_SERVICE
        ) as InputMethodManager
        val focusedView = this.requireActivity().currentFocus
        if (focusedView != null) {
            inputManager.hideSoftInputFromWindow(
                focusedView.windowToken,
                InputMethodManager.HIDE_NOT_ALWAYS
            )
        }
    }

    private fun searchfromSqlite(keyword: String) {
        if (keyword.isEmpty()) {
            dbcenter = DataHelper(requireActivity())
            db = dbcenter.readableDatabase
//        db.execSQL("DELETE FROM kegiatan WHERE tglygdiambil < datetime('now','-1 day','localtime')")
            cursor = db.rawQuery("SELECT * FROM products", null)

            cursor.moveToFirst()

            if (cursor.count > 0) {
                Log.e("cursor", cursor.count.toString())
                var datanya = ArrayList<dataproduk>()

                data.isVisible = true

                for (cc in 0 until cursor.count) {

                    cursor.moveToPosition(cc)
                    var listImg = ArrayList<String>()
                    val split = cursor.getString(11).split(",,")

                    for (i in split.indices) {
                        if (split[i] == "") {

                        } else {
                            listImg.add(split[i].trim())
                        }
                    }

                    datanya.add(
                        dataproduk(
                            cursor.getInt(1),
                            cursor.getString(2).replace("%", "'"),
                            cursor.getString(3).replace("%", "'"),
                            cursor.getInt(4),
                            cursor.getDouble(5),
                            cursor.getDouble(6),
                            cursor.getInt(7),
                            cursor.getString(8).replace("%", "'"),
                            cursor.getString(9).replace("%", "'"),
                            cursor.getString(10).replace("%", "'"),
                            listImg
                        )
                    )


//                    }


                }

                val adapter = AdapterProducts(datanya)
                data.setHasFixedSize(false)
                val layoutManager = GridLayoutManager(requireActivity(), 2)
                data.layoutManager = layoutManager
                data.suppressLayout(true)
                data.isNestedScrollingEnabled = true
                data.adapter = adapter


            } else {
                data.isVisible = false
                Toast.makeText(requireActivity(),"Products is Empty",Toast.LENGTH_SHORT).show()
            }
        } else {
            dbcenter = DataHelper(requireActivity())
            db = dbcenter.readableDatabase
//        db.execSQL("DELETE FROM kegiatan WHERE tglygdiambil < datetime('now','-1 day','localtime')")
            var k = keyword.toLowerCase()
            cursor = db.rawQuery("SELECT * FROM "
                    + "products" + " WHERE " + "title" + " LIKE '%" + k
                    + "%'", null)

            cursor.moveToFirst()

            if (cursor.count > 0) {
                Log.e("cursor", cursor.count.toString())
                var datanya = ArrayList<dataproduk>()
                data.isVisible = true

                for (cc in 0 until cursor.count) {
                    cursor.moveToPosition(cc)
                    var listImg = ArrayList<String>()

                    val split = cursor.getString(11).split(",,")

                    for (i in split.indices) {
                        if (split[i] == "") {

                        } else {
                            listImg.add(split[i].trim())
                        }
                    }

                    datanya.add(
                        dataproduk(
                            cursor.getInt(1),
                            cursor.getString(2).replace("%", "'"),
                            cursor.getString(3).replace("%", "'"),
                            cursor.getInt(4),
                            cursor.getDouble(5),
                            cursor.getDouble(6),
                            cursor.getInt(7),
                            cursor.getString(8).replace("%", "'"),
                            cursor.getString(9).replace("%", "'"),
                            cursor.getString(10).replace("%", "'"),
                            listImg
                        )
                    )


//                    }


                }

                val adapter = AdapterProducts(datanya)
                data.setHasFixedSize(false)
                val layoutManager = GridLayoutManager(requireActivity(), 2)
                data.layoutManager = layoutManager
                data.suppressLayout(true)
                data.isNestedScrollingEnabled = true
                data.adapter = adapter


            } else {
                data.isVisible = false
                Toast.makeText(requireActivity(),"Products is Empty",Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
    }

    companion object {
        const val TAG = "MenuFragment"
    }
}