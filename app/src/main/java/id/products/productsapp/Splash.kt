package id.news.app

import android.Manifest
import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.provider.Settings
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import id.products.productsapp.MainActivity
import id.products.productsapp.R

class Splash : AppCompatActivity() {

    var x: String = ""
    var SPLASH_TIME_OUT = 2500

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        window.statusBarColor = resources.getColor(R.color.biru)

        if (checkAndRequestPermissions()) {
            x = ""
            next()

        }

    }

    private fun checkAndRequestPermissions(): Boolean {
        val internet =
            ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_NETWORK_STATE)
        val listPermissionsNeeded: MutableList<String> = ArrayList()
        if (internet != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.ACCESS_NETWORK_STATE)
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toTypedArray(), 1)
        } else {
            next()
        }
        return false
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == 1) {
            if (grantResults.size > 0) {
                var sign: String
                sign = ""
                for (i in permissions.indices) {
                    if (permissions[i] == Manifest.permission.ACCESS_NETWORK_STATE) {
                        if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                            sign = "Y"
                        } else {
                            sign = "N"
                            val alertDialog2 = AlertDialog.Builder(this@Splash)
                            alertDialog2.setIcon(R.mipmap.ic_launcher_round)
                            alertDialog2.setTitle("Application Error")
                            alertDialog2.setMessage("Sorry...you have to give network and internet permissions so the application can run smoothly !")
                            alertDialog2.setCancelable(false)
                            if (ActivityCompat.shouldShowRequestPermissionRationale(
                                    this,
                                    Manifest.permission.ACCESS_NETWORK_STATE
                                )
                            ) {
                                alertDialog2.setPositiveButton(
                                    "OK"
                                ) { dialog, which -> checkAndRequestPermissions() }
                            } else {
                                alertDialog2.setPositiveButton(
                                    "OK"
                                ) { dialog, which ->
                                    val intent =
                                        Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
                                    val uri = Uri.fromParts(
                                        "package",
                                        packageName, null
                                    )
                                    intent.data = uri
                                    startActivityForResult(intent, 1)
                                    finish()
                                }
                            }
                            alertDialog2.show()
                            break
                        }
                    }
                }
                if (sign == "Y") {
                    x = ""
                    next()
                }
            }
        }
    }

    private fun next() {
        x = "sudah"
        Handler().postDelayed({
            val i = Intent(this@Splash, MainActivity::class.java)
            startActivity(i)
            overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_down)
            finish()
        }, SPLASH_TIME_OUT.toLong())
    }


}